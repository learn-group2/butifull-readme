#Wednesday
|     Time      |              occupation            | 
| ------------- |:----------------------------------:| 
| 8:00          | Waking up and doing routine        |
| 8:30          | eating breakfast                   |
| 9:10          | Studying **Physics**               |
| 11:00         | Studying **Urzadzenie techniki komputerowej**|
| 12:40         | Having free time                   |
| 13:55         | Studying **Math**                  |
| 14:50         | Studying **English**               |
| 15:45         | Having **Class time**              |
| 17:00         | Eating Dinner                      |
| 17:30         | Having free time                   |
| 23:00         | Doing evening routine              |
| 24:00         | Going to sleep                     |
