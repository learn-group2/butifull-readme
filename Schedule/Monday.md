#Monday
|     Time      |              occupation            | 
| ------------- |:----------------------------------:| 
| 7:00          | Waking up and doing routine        |
| 7:30          | eating breakfast                   |
| 8:15          | Studying **PM**                    |
| 10:05         | Studying **Administracja systemami operacyjnymi**                           | 
| 11:55         | Studying **Polish**                |
| 13:00         | Having free time                   |
| 15:00         | Eating Dinner                      |
| 17:00         | Going to Shop                      |
| 23:00         | Doing evening routine              |
| 24:00         | Going to sleep                     |
