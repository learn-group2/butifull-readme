#Tuesday
|     Time      |              occupation            | 
| ------------- |:----------------------------------:| 
| 6:30          | Waking up and doing routine        |
| 6:50          | eating breakfast                   |
| 7:20          | Studying **English**               |
| 8:15          | Studying **Math**                  |
| 9:10          | Studying **Systemy operacyjne**    |
| 10:05         | Studying **Creativity**            |
| 11:00         | Studying **Kompetencje spoleczne** |
| 11:00         | Studying **Polish**                |
| 13:00         | Studying **Lokalne sieci komputerowe**|
| 14:50         | Studying **Polish**                |
| 16:00         | Eating Dinner                      |
| 16:30         | Having free time                   |
| 23:00         | Doing evening routine              |
| 24:00         | Going to sleep                     |
