#Monday
|     Time      |              occupation            | 
| ------------- |:----------------------------------:| 
| 6:30          | Waking up and doing routine        |
| 6:50          | eating breakfast                   |
| 7:20          | Studying **Systemy operacyjne**    |
| 8:15          | Studying **Lokalne sieci komputerowe**|
| 10:05         | Studying **Math**                  |
| 10:50         | Having free time                   |
| 11:55         | Studying **Administracja systemami operacyjnymi**                           | 
| 13:00         | Studying **Deutsch**               |
| 15:00         | Eating Dinner                      |
| 17:00         | Going to Shop                      |
| 23:00         | Doing evening routine              |
| 24:00         | Going to sleep                     |
